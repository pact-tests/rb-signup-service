# frozen_string_literal: true

require_relative 'user'
require 'sinatra'
require 'securerandom'

class SignupController < Sinatra::Base
  def initialize(publisher = nil)
    super
    @publisher = publisher
  end

  post '/api/users' do
    email = JSON.parse(request.body.read)['e-mail']

    uuid = SecureRandom.uuid
    user = User.new uuid, email
    @publisher&.publish user

    status 201
    content_type 'application/json'
    user.to_json
  end
end
