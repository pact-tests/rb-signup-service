# frozen_string_literal: true

require 'json'

class User
  attr_reader :id, :email

  def initialize(id, email)
    @id = id
    @email = email
  end

  def ==(other)
    id == other.id && email == other.email
  end

  def to_json(*)
    { 'id' => id, 'e-mail' => email }.to_json
  end
end
