# frozen_string_literal: true

class RegistrationPublisher
  def initialize(queue)
    @queue = queue
  end

  def publish(user)
    message = { user_id: user.id }
    queue.publish(message)
  end

  private

  attr_reader :queue
end
