# Run the [Pact Broker](https://github.com/pact-foundation/pact_broker)

```
docker compose up -d
```

the broker should be available at http://localhost/

# Steps

## 1. Wallet Service - Registration Listener Spec

The `wallet service` listens for registration messages from the `sign up service` and creates a new wallet for the user.

Clone the [wallet service](https://gitlab.com/pact-tests/wallet-service) repo and implement the `registration_listener_spec.rb` test and publish the contract on the pact broker.

### usefull links

- example of [pact message consumer](https://github.com/pact-foundation/pact-message-ruby/blob/master/spec/features/create_message_pact_spec.rb#L78)
- [pact-message-ruby gem](https://github.com/pact-foundation/pact-message-ruby)

## 2. Signup Service - Wallet Service Contract Test

Make sure that this service produces registration messages that meets the expectation of the `wallet service` (aka the consumer).

a. Implement the `spec/wallet_service_contract_test.rb`

b. publish the result of the test on the broker.

```
bundle exec rake pact:verify
```

### useful links
- [pact-message-ruby gem](https://github.com/pact-foundation/pact-message-ruby)

## 3. Cart Service - Wallet Service Test

The `cart service` interacts with the `wallet service` for charging an user for an order checkout.

Clone the [cart service](https://gitlab.com/pact-tests/rb-cart-service) repo and implement the `wallet_client_spec.rb` test and publish the contract on the pact broker.

### useful links
- [pact-ruby gem](https://github.com/pact-foundation/pact-ruby)

## 4. Wallet Service - Cart Service Contract Test

Make sure that `wallet service` answers to the api calls in a way that meets the expectation of the `cart service` (aka the consumer).

## I'm Stuck!!!

Have a look at the `working_solution` branch available for each repo.

## TODO

- Add queues
- Error cases
- Create rake task to run unit and pact testsg