# frozen_string_literal: true

require_relative '../lib/user'
require 'rspec'

RSpec.describe User do
  context 'with a different e-mail' do
    it 'is considered different' do
      user1 = User.new 1234, 'user1@domain.com'
      user2 = User.new 1234, 'user2@domain.com'

      expect(user1).not_to eq user2
    end
  end
end
