# frozen_string_literal: true

ENV['APP_ENV'] = 'test'

require_relative '../lib/signup_controller'
require_relative '../lib/user'
require 'rspec'
require 'rack/test'
require 'securerandom'

RSpec.describe SignupController do
  include Rack::Test::Methods
  let(:publisher) { double('Publisher') }

  def app
    allow(publisher).to receive(:publish)
    SignupController.new publisher
  end

  context 'post /api/users' do 

    before do
      allow(SecureRandom).to receive(:uuid).and_return('1234')
      post '/api/users', '{"e-mail":"another@test.com"}', { 'CONTENT_TYPE' => 'application/json' }
    end

    it 'creates a user 1234' do
      expect(last_response).to be_created
      expect(last_response.content_type).to eq 'application/json'
      expect(last_response.body).to eq '{"id":"1234","e-mail":"another@test.com"}'
    end

    it 'notifies when a user is created' do
      user = User.new '1234', 'another@test.com'
      expect(publisher).to have_received(:publish).with(user)
    end
  end
end