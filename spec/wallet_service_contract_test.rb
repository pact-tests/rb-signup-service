# frozen_string_literal: true

require 'pact'
require 'pact/message'
require_relative '../lib/user'
require_relative '../lib/registration_publisher'

Pact.message_provider 'Signup Provider' do
  publish_verification_results true
  app_version '1.0'

end

class SpyQueue
  def publish(message)
    @message = message
  end

  def sent_message
    @message
  end
end
